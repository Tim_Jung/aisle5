package com.dunrite.aisle5;

import android.test.AndroidTestCase;

/**
 * Created by Zack on 2/14/2015.
 */
public class TestMyListMgr extends AndroidTestCase {
//    protected MyListItem item1 = new MyListItem();
//    protected MyListItem item2 = new MyListItem("milk steak", "dairy#meat");
//    protected MyListMgr list1 = new MyListMgr();
//    protected MyListMgr list2;
//    /**
//     * Tests methods in MyListMgr class.
//     */
//    public void testSizeMLM() {
//        assertEquals(list1.size(), 0);
//        ArrayList<MyListItem> arr1 = new ArrayList<>();
//        arr1.add(item1);
//        list2 = new MyListMgr(arr1);
//        assertEquals(list2.size(), 1);
//    }
//
//    public void testClearAll(){
//        assertEquals(list1.size(), 0);
//        ArrayList<MyListItem> arr1 = new ArrayList<>();
//        arr1.add(item1);
//        arr1.add(item2);
//        list2 = new MyListMgr(arr1);
//        assertEquals(list2.size(), 2);
//        list2.clearAll();
//        assertEquals(list2.size(), 0);
//    }
//
//    public void testItemPresentMLM() {
//        assertTrue(!list1.itemPresent(""));
//        ArrayList<MyListItem> arry1 = new ArrayList<>();
//        arry1.add(item1);
//        list2 = new MyListMgr(arry1);
//        assertTrue(list2.itemPresent(""));
//        arry1.add(item2);
//        assertTrue(list2.itemPresent(""));
//        assertTrue(list2.itemPresent("milk steak"));
//    }
//
//    public void testGetMyItemMLM(){
//        ArrayList<MyListItem> arry1 = new ArrayList<>();
//        arry1.add(item1);
//        arry1.add(item2);
//        list2 = new MyListMgr(arry1);
//        assertEquals(list2.getMyItem(item1.getName()).getName(), item1.getName());
//        assertEquals(list2.getMyItem(item1.getName()).getCategory(), item1.getCategory());
//        assertEquals(list2.getMyItem(item2.getName()).getName(), item2.getName());
//        assertEquals(list2.getMyItem(item2.getName()).getCategory(), item2.getCategory());
//    }
//
//    public void testAddItemMLM() {
//        list2 = new MyListMgr();
//        assertEquals(list2.size(), 0);
//        list2.addItem(item1.getName(), item1.getCategory());
//        assertEquals(list2.size(), 1);
//        list2.addItem(item2.getName(), item2.getCategory());
//        assertEquals(list2.size(), 2);
//        assertTrue(list2.itemPresent(""));
//        assertTrue(list2.itemPresent("milk steak"));
//        list2.addItem(item2.getName(), item2.getCategory());
//        assertEquals(list2.size(), 2);
//        assertEquals(list2.getMyItem(item2.getName()).getQuantity(), 2);
//    }
//
//    public void testRemoveItem(){
//        ArrayList<MyListItem> arry1 = new ArrayList<>();
//        arry1.add(item1);
//        arry1.add(item2);
//        list2 = new MyListMgr(arry1);
//        assertEquals(list2.size(), 2);
//        assertTrue(list2.itemPresent(""));
//        list2.removeItem("");
//        assertTrue(!list2.itemPresent(""));
//        assertEquals(list2.size(), 1);
//        list2.removeItem("");
//        assertEquals(list2.size(), 1);
//        list2.removeItem("milk steak");
//        assertEquals(list2.size(), 0);
//    }
//
//    public void testGetMyListMLM() {
//        ArrayList<MyListItem> arry1 = new ArrayList<>();
//        arry1.add(item1);
//        arry1.add(item2);
//        list2 = new MyListMgr(arry1);
//        assertTrue(list2.itemPresent(""));
//        assertTrue(list2.itemPresent("milk steak"));
//        MyListMgr templist = new MyListMgr(list2.getMyList());
//        assertTrue(templist.itemPresent(""));
//        assertTrue(templist.itemPresent("milk steak"));
//    }
//
}
