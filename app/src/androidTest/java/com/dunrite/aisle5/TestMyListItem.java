package com.dunrite.aisle5;

import android.test.AndroidTestCase;

import com.dunrite.aisle5.list_managment.MyListItem;


/**
 * Created by ForExampleJohn on 2/7/2015.
 */
public class TestMyListItem extends AndroidTestCase {
    protected MyListItem item1 = new MyListItem();
    protected MyListItem item2 = new MyListItem("milk steak", "dairy#meat");


    /**
     * Tests all getters and setters for MyListItem Class
     */
    public void testGetNametMLI() {
        assertEquals(item1.getName(), "");
        assertEquals(item2.getName(), "milk steak");
    }

    public void testSetNameMLI() {
        item1.setName("corn hole");
        assertEquals(item1.getName(), "corn hole");
    }

    public void testGetCategoryMLI() {
        assertEquals(item1.getCategory(), "");
        assertEquals(item2.getCategory(), "dairy#meat");
    }

    public void testSetCategoryMLI() {
        item1.setCategory("cornIGuess?");
        assertEquals(item1.getCategory(), "cornIGuess?");
    }

    public void testGetNotesMLI() {
        assertEquals(item1.getNotes(), null);
    }

    public void testSetNotesMLI() {
        item1.setNotes("All dem apples");
        assertEquals(item1.getNotes(), "All dem apples");
    }

    public void testGetQuantityMLI() {
        assertEquals(item1.getQuantity(), 1);
    }

    public void testSetQuantityMLI() {
        item1.setQuantity(2);
        assertEquals(item1.getQuantity(), 2);
    }

    public void testIsCheckedOff() { assertTrue(!item1.isCheckedOff());}

    public void testCheckOff(){
        assertTrue(!item1.isCheckedOff());
        item1.checkOff();
        assertTrue(item1.isCheckedOff());
    }


    /**
     * Test Additional methods of MyListItem class.
     */
    public void testIncrementQuantityMLI() {
        item1.setQuantity(1);
        item1.incrementQuantity();
        assertEquals(item1.getQuantity(), 2);
        item1.incrementQuantity();
        assertEquals(item1.getQuantity(), 3);
        item1.setQuantity(5);
        item1.incrementQuantity();
        assertEquals(item1.getQuantity(), 6);
    }
}
