package com.dunrite.aisle5;

import android.test.AndroidTestCase;

import com.dunrite.aisle5.list_managment.QuickListItem;

/**
 * Created by Zack on 2/14/2015.
 */
public class TestQuickListItem extends AndroidTestCase {
    protected QuickListItem item3 = new QuickListItem();
    protected QuickListItem item4 = new QuickListItem("milk steak", "dairy#meat");
    /**
     * Tests all getters and setters for QuickListItem Class
     */
    public void testGetNametQLI() {
        assertEquals(item3.getName(), "");
        assertEquals(item4.getName(), "milk steak");
    }

    public void testSetNameQLI() {
        item3.setName("corn hole");
        assertEquals(item3.getName(), "corn hole");
    }

    public void testGetCategoryQLI() {
        assertEquals(item3.getCategory(), "");
        assertEquals(item4.getCategory(), "dairy#meat");
    }

    public void testSetCategoryQLI() {
        item3.setCategory("cornIGuess?");
        assertEquals(item3.getCategory(), "cornIGuess?");
    }

    public void testGetScoreQLI() {
        assertEquals(item3.getScore(), 0);
        assertEquals(item4.getScore(), 1);
    }

    public void testSetScoreQLI() {
        item3.setScore(1);
        assertEquals(item3.getScore(), 1);
    }

    /**
     * Tests additional methods in QuickListItem class.
     */
    public void testIncrementScoreQLI() {
        assertEquals(item4.getScore(), 1);
        item4.incrementScore();
        assertEquals(item4.getScore(), 2);
        item4.setScore(5);
        item4.incrementScore();
        assertEquals(item4.getScore(), 6);
    }
}
