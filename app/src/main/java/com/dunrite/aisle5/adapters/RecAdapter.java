package com.dunrite.aisle5.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dunrite.aisle5.R;
import com.dunrite.aisle5.activities.MainActivity;
import com.dunrite.aisle5.list_managment.MyListItem;
import com.dunrite.aisle5.list_managment.MyListMgr;
import com.dunrite.aisle5.list_managment.QuickListMgr;
import com.dunrite.aisle5.util.SnackUtil;
import com.dunrite.aisle5.util.Utils;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.listeners.ActionClickListener;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Handles the dynamic population of the grid of item cards, depending on which category
 * is currently selected.
 */
public class RecAdapter extends RecyclerView.Adapter<RecAdapter.ViewHolder> {
    private static String[] mDataset;
    private final Activity context;
    public static boolean inMyList;
    public static float coordX, coordY;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements
            OnClickListener, OnLongClickListener, OnTouchListener {
        public ImageView mImageView, check, note;
        public TextView mTextView, itemCount;


        public ViewHolder(View v) {
            super(v);
            mImageView = (ImageView) v.findViewById(R.id.bg_image);
            check = (ImageView) v.findViewById(R.id.check);
            note = (ImageView) v.findViewById(R.id.notes);
            check.setOnClickListener(this);
            mImageView.setOnClickListener(this);
            mImageView.setOnLongClickListener(this);
            mImageView.setOnTouchListener(this);
            mTextView = (TextView) v.findViewById(R.id.itemName);
            itemCount = (TextView) v.findViewById(R.id.itemCount);
        }

        /**
         * Helps the detailsview animation see where the user's touch was located.
         */
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                coordX = event.getRawX();
                coordY = event.getRawY();
            }
            return false;
        }

        @Override
        public void onClick(View v) {
            QuickListMgr quickList =QuickListMgr.getInstance();
            final MyListMgr myList = MyListMgr.getInstance();
            final String itemName = mTextView.getText().toString();

            if (!inMyList) { //Only allow item adding if not in MyList
                //QuickList add call has to come first
                final Pair<String, Integer> snackValues;
                quickList.addItem(itemName, MainActivity.MyFragment.getCat()); //Adds item to QuickList
                snackValues = myList.addItem(itemName, MainActivity.MyFragment.getCat()); //Adds item to MyList
                String snackText = "Added " + snackValues.first + "\n" + snackValues.second + " total";

                //Multi-line Snackbar that displays "Added ITEM.\n # Total" with the option to undo the action
                SnackUtil.actionSnack(context, true, snackText, "Undo", new ActionClickListener() {
                    @Override
                    public void onActionClicked(Snackbar snackbar) {
                        int prevCount = myList.getMyItem(snackValues.first).getQuantity();
                        //Checks if this is the first time adding this item to MyList
                        if (prevCount != 0)
                            myList.getMyItem(snackValues.first).setQuantity(prevCount - 1);
                        else
                            myList.removeItem(snackValues.first);
                    }
                });
                Utils.prefSave(context);
            } else {
                if (check.getVisibility() == View.INVISIBLE) {
                    myList.getMyItem(itemName).checkOff();
                    check.setVisibility(View.VISIBLE);
                } else {
                    myList.getMyItem(itemName).checkOff();
                    check.setVisibility(View.INVISIBLE);
                }
                Utils.prefSave(context);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (inMyList) {
                MainActivity.MyFragment.setDetails(mTextView.getText().toString(), itemCount.getText().toString());
            }
            return true;
        }


    }

    //Allow external classes to set boolean inMyList
    public void setInMyList(Boolean p) {
        inMyList = p;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecAdapter(Activity context, String[] myDataset) {
        mDataset = myDataset;
        this.context = context;
    }

    //Used by swipe listener to remove a specific item
    public void remove(int pos) {
        ArrayList<String> temp = new ArrayList<>(Arrays.asList(mDataset));
        temp.remove(pos);
        mDataset = new String[temp.size()];
        mDataset = temp.toArray(new String[temp.size()]);
        Utils.prefSave(context);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                    int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.card, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        //displays the Item Name
        holder.mTextView.setText(mDataset[position]);
        ArrayList<MyListItem> myList = MyListMgr.getInstance().getMyList();

        //displays item count in upper right of card if in MyList and count > 1
        //also displays checkmark if checked
        //also displays notes icon if notes are present
        if (inMyList) {
            holder.itemCount.setVisibility(View.VISIBLE);
            for (MyListItem q : myList) {
                if (q.getName().equals(mDataset[position])) {
                    holder.itemCount.setText(Integer.toString(q.getQuantity()));
                    if (q.getQuantity() < 2)
                        holder.itemCount.setVisibility(View.INVISIBLE);
                    if (!("".equals(q.getNotes()) || q.getNotes() == null))
                        holder.note.setVisibility(View.VISIBLE);
                    if (q.isCheckedOff())
                        holder.check.setVisibility(View.VISIBLE);
                    else
                        holder.check.setVisibility(View.INVISIBLE);
                }
            }
        } else {
            holder.itemCount.setVisibility(View.INVISIBLE); //not in MyList. Hide count
        }
        int pic = context.getResources().getIdentifier(holder.mTextView.getText().toString().toLowerCase(), "drawable", context.getPackageName());
        if(pic == 0){
            pic = context.getResources().getIdentifier("noart", "drawable", context.getPackageName());
        }
        //displays the Item Image in the card using Glide library
        Glide.with(context)
                .load(pic)
                .centerCrop()
                .into(holder.mImageView);

    }

    // Return the size of the dataset (invoked by the layout)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
