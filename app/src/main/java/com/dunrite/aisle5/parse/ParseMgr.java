package com.dunrite.aisle5.parse;

import android.util.Log;

import com.dunrite.aisle5.list_managment.MyListMgr;
import com.google.gson.Gson;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class houses all of the Parse management functionality.
 * Includes functionality such as invites, push notifications, cloud storage
 * and synchronization.
 */
public class ParseMgr {
    private static ParseMgr parseMgr = null;
    private String parseObjID;
    private String UserEmail = "";
    private ParseObject parseList = new ParseObject("parseListObject");
    private boolean isSyncing = false;
    private String ownersEmail = "";
    private ArrayList<String> sharingUsersList = new ArrayList<>();

    //private constructor ensures only the getInstance method
    //can construct an instance of this class.
    private ParseMgr(){
    }

    public static ParseMgr getInstance(){
        if (parseMgr == null) parseMgr = new ParseMgr();
        return parseMgr;
    }

    public static void setInstance(ParseMgr pMgr){
        parseMgr = pMgr;
    }

    public void setSharingUsersList(ArrayList<String> theList) { sharingUsersList = theList;}

    public ArrayList<String> getSharingUsersList(){ return sharingUsersList;}

    public void setParseObjID(String pID){
        parseObjID = pID;
    }

    public String getParseObjID(){
        return parseObjID;
    }

    public boolean syncStatus(){
        return isSyncing;
    }

    public void syncToggle(boolean set){
        isSyncing = set;
    }

    public void setEmail(String pEmail){
        UserEmail = pEmail;
    }

    private void setOwnersEmail(String pEmail){
        ownersEmail = pEmail;
    }

    public String getOwnersEmail(){
        return ownersEmail;
    }

    public ParseObject getParseList(){
        return parseList;
    }

    /**
     * Do basic check for valid e-mail addresses to give users some
     * feedback when an invalid address is entered.
     */
    public boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        //TODO: take a step further and check if in Parse DB
    }

    /**
     * This method saves the user myList to Parse.
     *
     * @param jsonObj is the obj that will be stored on Parse
     * @param callBack represents an obj containing code to call after saveInBackground completes
     */
    public void parseCreate(String jsonObj, final SaveCallback callBack) {
        //turn the ParseObject into a json obj
        parseList.put("parseList", jsonObj);
        parseList.put("usersGmail", UserEmail);
        Log.d("email", UserEmail);
        // Save the current Installation to Parse.
        ParseInstallation userInstallation = ParseInstallation.getCurrentInstallation();
        userInstallation.put("userEmail", UserEmail);
        userInstallation.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    parseList.put("installationIDs", Arrays.asList(ParseInstallation.getCurrentInstallation().getInstallationId()));
                    if (callBack == null)
                        parseList.saveInBackground();
                    else {
                        //SaveCallback ensures no race condition occurs.
                        parseList.saveInBackground(callBack);
                    }
                } else {
                    Log.d("Tim","creating the parse object didn't work");
                }
            }
        });
    }

    /**
     * This method updates the user myList on Parse.
     *
     * @param jsonObj is the obj that will be stored on Parse
     */
    public void parseUpdate(final String jsonObj) {
        final String temp = jsonObj;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("parseListObject");
        // Retrieve the object using the id
        query.getInBackground(parseObjID, new GetCallback<ParseObject>() {
            public void done(ParseObject p, ParseException e) {
                Log.d("UPDATE", parseObjID);
                if (e == null) {
                    if (jsonObj != null)
                        p.put("parseList", temp);
                    else
                        p.addAllUnique("invitedEmails", Arrays.asList(UserEmail));
                    p.saveInBackground();
                }
            }
        });
    }

    /**
     * This method is for pulling data from Parse.
     *
     * @param firstPull
     */
    public void parsePull(final boolean firstPull) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("parseListObject");
        final Gson gson = new Gson();
        query.getInBackground(parseObjID, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    String listJson = object.getString("parseList");
                    Log.d("listJson", listJson);
                    MyListMgr.setInstance(gson.fromJson(listJson, MyListMgr.class));
                    setOwnersEmail(object.getString("usersGmail"));
                    Log.d("ownersEmail", ownersEmail);
                    //if sharing your list with others, set isSyncing to true
                    //TODO: This is probably wrong
                    if (ownersEmail.equals(UserEmail) && object.get("invitedEmails") != null) {
                        isSyncing = true;
                    }
                    if(firstPull) {
                        parseSendNotification(ownersEmail, "accepted");
                    }
                } else {
                    Log.d("Tim","The data was not retrieved.");
                }
            }
        });
    }

    /**
     * This is for sending push notifications with Parse.
     *
     * @param email
     * @param action
     */
    public void parseSendNotification(final String email, final String action) {
        Log.d("Tim", "We called the send method");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("parseListObject");
        query.whereEqualTo("usersGmail", email);
        query.selectKeys(Arrays.asList("installationIDs"));
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    //if list is null or empty
                    if (list == null || list.isEmpty()) {
                        Log.d("Tim","The users email wasn't found");
                    } else { //if list is not null or empty
                        //Query for specific parse user
                        ParseQuery pushQuery = ParseInstallation.getQuery();
                        pushQuery.whereEqualTo("userEmail", email);
                        Log.d("isolateEmail", email);
                        //Do the push notification for the invite
                        ParsePush push = new ParsePush();
                        push.setQuery(pushQuery);
                        JSONObject data = new JSONObject();
                        try {
                            if (action.equals("accepted")) {
                                data.put("alert", UserEmail + " has accepted your invitation.");
                            }
                            if (action.equals("declined")) {
                                data.put("alert", UserEmail + " has declined your invitation.");
                            }
                            Log.d("TIM", "ABOUT TO PUSH");
                            push.setData(data);
                        } catch (JSONException j) {
                            Log.d("Tim","JsonException");
                        }
                        push.sendInBackground(new SendCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Log.d("TIM", "PUSH SUCCESSFUL");
                                } else {
                                    Log.d("TIM", "PUSH FAILED " + e.getMessage());
                                }
                            }
                        });
                    }
                } else {
                    //TODO: Handle error condition by starting a retry operation or showing error message to user
                }
            }
        });
    }

    /**
     * This method is getting the installationIDs for a user that the owner is inviting so
     * they can receive an invite push notification.
     */
    public List<String> sendInvitation(final String email) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("parseListObject");
        query.whereEqualTo("usersGmail", email);
        query.selectKeys(Arrays.asList("installationIDs"));
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    //if list is null or empty
                    if (list == null || list.isEmpty()) {
                        Log.d("Tim","The users email wasn't found");
                    } else { //if list is not null or empty
                        //Query for specific parse user
                        ParseQuery invitePushQuery = ParseInstallation.getQuery();
                        invitePushQuery.whereEqualTo("userEmail", email);
                        Log.d("isolateEmail", email);
                        //Do the push notification for the invite
                        ParsePush invitePush = new ParsePush();
                        invitePush.setQuery(invitePushQuery);
                        JSONObject data = new JSONObject();
                        try {
                            data.put("alert", "You have been invited to the grocery list of " + UserEmail + ".\nDo you accept?");
                            Log.d("OBJECTID", parseObjID);
                            data.put("theid", parseObjID);
                            invitePush.setData(data);
                        } catch (JSONException j) {
                            Log.d("Tim","JsonException");
                        }
                        invitePush.sendInBackground(new SendCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Log.d("TIM", "PUSH SUCCESSFUL");
                                } else {
                                    Log.d("TIM", "PUSH FAILED " + e.getMessage());
                                }
                            }
                        });
                    }
                } else {
                    //TODO: Handle error condition by starting a retry operation or showing error message to user
                }
            }
        });
        return null;
    }

    /**
     * Retrieves members in the list to display them in the "Sharing With" section.
     */
    public void retrieveSharingMembers() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("parseListObject");
        final Gson gson = new Gson();
        query.getInBackground(parseObjID, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    JSONArray listJson = object.getJSONArray("invitedEmails");
                    Log.d("sharingUsers", "" + listJson);
                    ArrayList<String> thisList = new ArrayList<>();
                    if (listJson != null) {
                        for (int i=0; i < listJson.length(); i++) {
                            try {
                                thisList.add(listJson.get(i).toString());
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    } else {
                        Log.d("Tim","List was null");
                    }
                    setSharingUsersList(thisList);
                    Log.d("sharingUsers2", "" + thisList);
                } else {
                    Log.d("Tim","The data was not retrieved");
                }
            }
        });
    }
}
