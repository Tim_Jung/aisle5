package com.dunrite.aisle5.parse;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.dunrite.aisle5.activities.InviteActivity;
import com.parse.ParsePushBroadcastReceiver;

/**
 * When user receives Parse invite, this decides what to do with it
 */
public class InvitationReceiver extends ParsePushBroadcastReceiver {
    @Override
    public void onPushOpen(Context context, Intent intent) {
        Log.d("Push", "Clicked");
        //Log.d("INTENT",intent.getBundleExtra("type").toString());
        Bundle extras = intent.getExtras();
        String JSONdata = extras.getString("com.parse.Data");
        Log.d("TIM", JSONdata);
        Intent i = new Intent(context, InviteActivity.class);
        i.putExtras(intent.getExtras());
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addCategory("invite");
        context.startActivity(i);
    }

    @Override
    protected Notification getNotification(Context context, Intent intent) {
        Log.d("TIM", "GETNOTIFICATION");
        return null;
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        Log.d("TIM", "ONPUSHRECEIVE");
        //Log.d("INTENT",intent.getDataString());
    }
}
