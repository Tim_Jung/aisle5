package com.dunrite.aisle5.parse;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;

/**
 * Required for connecting to Parse cloud database
 */
public class AisleFiveApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Initializes Parse to be used
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "Sp52LuRUaUFxNgRJqTYPoHUZeZvpesEMDTgeUFuw"
                , "OXccJTOQ58a0OPD5sbvWsHLYWYcFSv5JxnMgPLsJ");

        //For push notification registration
        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }
}
