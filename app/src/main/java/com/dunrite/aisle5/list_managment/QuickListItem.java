package com.dunrite.aisle5.list_managment;

/**
 * Item that will be stored inside of OuickList, which consists of the user's
 * most frequently added items.
 */
public class QuickListItem extends Item implements Comparable<QuickListItem> {
    private int score;

    /**
     * Default Constructor.
     */
    public QuickListItem() {
        score = 0;
    }

    /**
     * Constructor to specify data.
     *
     * @param pName is the name of the grocery item
     * @param pCategories is the one or two categories
     */
    public QuickListItem(String pName, String pCategories) {
        name = pName;
        score = 1;
        category = pCategories;
    }

    /**
     * Constructor to specify data.
     *
     * @param pName is the name of the grocery item
     * @param pCategories is the one or two categories
     * @param pScore is the score of the grocery item
     */
    public QuickListItem(String pName, String pCategories, int pScore) {
        name = pName;
        score = pScore;
        category = pCategories;
    }

    /**
     * Increments the score of the QuickListItem.
     */
    public void incrementScore() {
        score++;
    }

    /**
     * Getters & Setters.
     */
    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Needed by Collections.sort to order the QuickList.
     *
     * @param that the item being compared to this item
     * @return the int used by Collections.sort to order the ArrayList appropriately
     */
    @Override
    public int compareTo(QuickListItem that) {
        return that.getScore() - this.getScore();
    }
}
