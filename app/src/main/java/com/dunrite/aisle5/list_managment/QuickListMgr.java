package com.dunrite.aisle5.list_managment;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Manages the QuickList by handling the adding of items in a PriorityQueue of OuickListItems.
 * QuickList holds all used items sorted by most frequently added
 */
public class QuickListMgr {
    private ArrayList<QuickListItem> quickList;
    private static QuickListMgr quickListMgr = null;
    /**
     * Default constructor.
     */
    private QuickListMgr() {
        quickList = new ArrayList<>();
    }
    /**
     * Constructor for taking prebuilt tree.
     */
    public QuickListMgr(ArrayList<QuickListItem> pList) {
        quickList = new ArrayList<>();
        quickList = pList;
    }

    public static QuickListMgr getInstance(){
        if (quickListMgr == null) quickListMgr = new QuickListMgr();

        return quickListMgr;
    }

    public static void setInstance(QuickListMgr pListMgr){
        quickListMgr = pListMgr;
    }

    /**
     * Decides whether to add an item, increment its score, or do nothing.
     *
     * @param pName is the name of the grocery item
     * @param pCategory is category of the grocery item
     */
    public void addItem(String pName, String pCategory) {
        boolean itemIsPresent = false;
        //checks if the item is in MyList already or not
        if (!MyListMgr.getInstance().itemPresent(pName)) {
            for (QuickListItem q : quickList) {
                if (q.getName().equals(pName)) {
                    q.incrementScore();
                    itemIsPresent = true;
                    break;
                }
            }
            //if the item wasn't present we need to make it and add it.
            if (!itemIsPresent) {
                QuickListItem thisItem = new QuickListItem(pName, pCategory);
                quickList.add(thisItem);
            }
            //ensures the list appears in order of highest score first to lowest score last.
            Collections.sort(quickList);
        }
    }

    /**
     * For getting the size of quickList.
     *
     * @return int the size of the quickList
     */
    public int size() {
        return quickList.size();
    }

    public boolean itemPresent(String pName) {
        boolean present = false;
        if (!(getQuickItem(pName) == null)) present = true;
        return present;
    }

    /**
     * Returns Item with specified name.
     * Returns null if item not present.
     *
     * @param pName is the name of the grocery item
     * @return item
     */
    public QuickListItem getQuickItem(String pName) {
        QuickListItem item = null;
        for (QuickListItem q : quickList) {
            if (q.getName().equals(pName)) {
                item = q;
            }
        }
        return item;
    }
    /**
     * Retrieves the entire quickList ArrayList.
     *
     * @return quickList
     */
    public ArrayList<QuickListItem> getQuickList() {
        return quickList;
    }
}
