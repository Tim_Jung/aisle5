package com.dunrite.aisle5.list_managment;

/**
 * An item that will be inside of the user's grocery list (MyList).
 */
public class MyListItem extends Item implements Comparable<MyListItem> {
    private String notes;
    private int quantity;
    private boolean checked;

    /**
     * Default Constructor.
     */
    public MyListItem() {
        notes = null;
        quantity = 1;
        checked = false;
    }

    /**
     * Constructor to specify data without score.
     *
     * @param pName is the name of the grocery item
     * @param pCategories is the one or two categories
     */
    public MyListItem(String pName, String pCategories) {
        name = pName;
        category = pCategories;
        notes = null;
        quantity = 1;
        checked = false;
    }

    public void incrementQuantity() {
        quantity++;
    }

    /**
     * Getters & Setters.
     */
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public boolean isCheckedOff() { return checked; }
    public void checkOff() {
        checked = !checked;
    }

    /**
     * Needed by Collections.sort to order the QuickList.
     *
     * @param that the item being compared to this item
     * @return the int used by Collections.sort to order the ArrayList appropriately
     */
    @Override
    public int compareTo(MyListItem that) {
        return this.getName().compareTo(that.getName());
    }
}
