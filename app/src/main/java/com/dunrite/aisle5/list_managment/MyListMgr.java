package com.dunrite.aisle5.list_managment;

import android.util.Pair;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Manages MyList by handling the adding and removing of items in a PriorityQueue of MyListItems.
 * MyList is the user's grocery list of items they need from the store
 */
public class MyListMgr {
    private ArrayList<MyListItem> myList;
    private static MyListMgr myListMgr = null;

    /**
     * Default constructor.
     */
    private MyListMgr() {
        myList = new ArrayList<>();
    }

    /**
     * Constructor for taking prebuilt tree.
     */
    public MyListMgr(ArrayList<MyListItem> pList) {
        myList = new ArrayList<>();
        myList = pList;
    }

    public static MyListMgr getInstance(){
        if (myListMgr == null) myListMgr = new MyListMgr();

        return myListMgr;
    }

    public static void setInstance(MyListMgr pListMgr){
        myListMgr = pListMgr;
    }

    /**
     * Decides whether to add an item, increment its score, or do nothing.
     *
     * @param pName     is the name of the grocery item
     * @param pCategory is category of the grocery item
     */
    public Pair<String, Integer> addItem(String pName, String pCategory) {
        boolean itemIsPresent = false;
        MyListItem curr = new MyListItem();
        for (MyListItem q : myList) {
            if (q.getName().equals(pName)) {
                q.incrementQuantity();
                curr.setName(q.getName());
                curr.setQuantity(q.getQuantity());
                curr.setNotes(q.getNotes());
                itemIsPresent = true;
                break;
            }
        }
        //if the item wasn't present we need to make it and add it.
        if (!itemIsPresent) {
            MyListItem thisItem = new MyListItem(pName, pCategory);
            myList.add(thisItem);
        }
        //ensures the list appears in order of highest score first
        // to lowest score last.
        Collections.sort(myList);

        return new Pair<>(pName, curr.getQuantity());
    }

    /**
     * For getting the size of myList.
     *
     * @return int the size of the myList
     */
    public int size() {
        return myList.size();
    }

    public boolean itemPresent(String pName) {
        boolean present = false;
        if (!(getMyItem(pName) == null)) present = true;
        return present;
    }

    /**
     * Returns Item with specified name.
     * Returns null if item not present.
     *
     * @param pName is the name of the grocery item
     * @return item
     */
    public MyListItem getMyItem(String pName) {
        MyListItem item = null;
        for (MyListItem q : myList) {
            if (q.getName().equals(pName)) {
                item = q;
            }
        }
        return item;
    }

    /**
     * Removes the MyListItem from the TreeSet myList.
     *
     * @param pName is the name of the grocery item
     */
    public void removeItem(String pName) {
        for (MyListItem q : myList) {
            if (q.getName().equals(pName)) {
                myList.remove(q);
                break;
            }
        }
    }

    /**
     * Retrieves the entire myList ArrayList.
     *
     * @return myList
     */
    public ArrayList<MyListItem> getMyList() {
        return myList;
    }

    /**
     * Clears the entire myList ArrayList.
     */
    public void clearAll() {
        myList.clear();
    }

}
