package com.dunrite.aisle5.list_managment;

/**
 * Parent class for QuickListItem and MyListItem.
 */
public abstract class Item {
    protected String name, category;

    /**
     * Default Constructor.
     */
    protected Item() {
        name = "";
        category = "";
    }

    /**
     * Setters & Getters.
     */
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
