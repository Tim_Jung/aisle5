package com.dunrite.aisle5.util;

import android.content.Context;
import android.graphics.Color;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.nispok.snackbar.listeners.ActionClickListener;

/**
 * This class makes creating snackbars throughout the application much
 * easier, simplifying the code at the same time.
 */
public class SnackUtil {

    private SnackUtil() {
        //empty constructor
    }

    /**
     * A normal blue snackbar with no action attached
     */
    public static void snack(Context c, boolean isMulti, String message) {
        SnackbarType type;
        if (isMulti)
            type = SnackbarType.MULTI_LINE;
        else
            type = SnackbarType.SINGLE_LINE;
        SnackbarManager.show(
                Snackbar.with(c)
                        .type(type)
                        .text(message)
                        .color(Color.parseColor("#3f51b5")));
    }

    /**
     * A normal blue snackbar with an action and action listener
     */
    public static void actionSnack(Context c, boolean isMulti, String message, String action, ActionClickListener click) {
        SnackbarType type;
        if (isMulti)
            type = SnackbarType.MULTI_LINE;
        else
            type = SnackbarType.SINGLE_LINE;
        SnackbarManager.show(
                Snackbar.with(c)
                        .type(type)
                        .text(message)
                        .color(Color.parseColor("#3f51b5"))
                        .actionLabel(action)
                        .actionListener(click));
    }

    /**
     * A snackbar colored red as a warning message
     */
    public static void warningSnack(Context c, boolean isMulti, String message) {
        SnackbarType type;
        if (isMulti)
            type = SnackbarType.MULTI_LINE;
        else
            type = SnackbarType.SINGLE_LINE;
        //Multi-line Snackbar that displays "Added ITEM.\n # Total" with the option to undo the action
        SnackbarManager.show(
                Snackbar.with(c)
                        .type(type)
                        .text(message)
                        .color(Color.parseColor("#f53d37")));
    }

    /**
     * This is a snackbar colored red as a warning with an action and action listener
     */
    public static void warningActionSnack(Context c, boolean isMulti, String message, String action, ActionClickListener click) {
        SnackbarType type;
        if (isMulti)
            type = SnackbarType.MULTI_LINE;
        else
            type = SnackbarType.SINGLE_LINE;
        SnackbarManager.show(
                Snackbar.with(c)
                        .type(type)
                        .text(message)
                        .color(Color.parseColor("#f53d37"))
                        .actionLabel(action)
                        .actionListener(click));
    }
}