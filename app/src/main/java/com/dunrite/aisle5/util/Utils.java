package com.dunrite.aisle5.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dunrite.aisle5.list_managment.MyListMgr;
import com.dunrite.aisle5.list_managment.QuickListMgr;
import com.dunrite.aisle5.parse.ParseMgr;
import com.google.gson.Gson;
import com.parse.ParseException;
import com.parse.SaveCallback;

/**
 * Utility Class for utilities, obviously
 */
public final class Utils {

    private Utils() {
    }

    /**
     * Saves objects in the user preferences.
     */
    public static void prefSave(final Context c) {
        //Log.d("prefSave","Im being called");
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(c);
        final Gson gson = new Gson();
        final SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        //store them in the user preferences
        String myListJson2 = gson.toJson(MyListMgr.getInstance());
        String quickListJson2 = gson.toJson(QuickListMgr.getInstance());
        String isSyncingJson2 = gson.toJson(ParseMgr.getInstance().syncStatus());
        prefsEditor.putString("myList", myListJson2);
        prefsEditor.putString("quickList", quickListJson2);
        prefsEditor.putString("isSyncing", isSyncingJson2);
        //When we don't have the ID yet
        if (ParseMgr.getInstance().getParseObjID() == null) {
            ParseMgr.getInstance().parseCreate(myListJson2, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        ParseMgr.getInstance().setParseObjID(ParseMgr.getInstance().getParseList().getObjectId());
                        String parseObjectIDJson = gson.toJson(ParseMgr.getInstance().getParseObjID());
                        Log.d("JSONID", "" + parseObjectIDJson);
                        prefsEditor.putString("parseObjectID", parseObjectIDJson);
                        prefsEditor.apply();
                    } else {
                        //There's a bunch of ways it could fail so...
                        //parse hicups, no internet access, etc.
                        SnackUtil.warningSnack(c, false, "No internet access. Sync failed.");
                        //TODO: re-execute the code at a later time.
                    }
                }
            });
            //Log.d("parseCreate","ITS NULL");
        } else {
            ParseMgr.getInstance().parseUpdate(myListJson2);
            String parseObjectIDJson = gson.toJson(ParseMgr.getInstance().getParseObjID());
            prefsEditor.putString("parseObjectID", parseObjectIDJson);
            prefsEditor.apply();
            //Log.d("parseUpdate", "ITS NOT NULL");
        }
    }
}
