package com.dunrite.aisle5.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.dunrite.aisle5.R;
import com.dunrite.aisle5.parse.ParseMgr;
import com.dunrite.aisle5.util.SnackUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class InviteActivity extends AppCompatActivity {
    public String newID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        //Handle the invite intent
        Bundle extras = getIntent().getExtras();
        if (extras != null && getIntent().hasCategory("invite")) {
            String JSONdata = extras.getString("com.parse.Data");
            try {
                JSONObject json = new JSONObject(JSONdata);
                newID = json.getString("theid");
            } catch (JSONException j) {
                //TODO: Oh god how did we even get here?
            }
        }

        //TODO: Show actual email that is sharing with user
        getSupportActionBar().setTitle("Invite Received");
        final Button yesButton = (Button) findViewById(R.id.yesButton);
        final Button noButton = (Button) findViewById(R.id.noButton);
        View.OnClickListener z = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (v == yesButton) {
                    //set their currentID to the new ID
                    ParseMgr.getInstance().setParseObjID(newID);
                    ParseMgr.getInstance().syncToggle(true);
                    //dump their MyList and adopt the new one
                    ParseMgr.getInstance().parsePull(true);
                    //adds user's email to invitedEmails in the parseListObj
                    ParseMgr.getInstance().parseUpdate(null);
                    //return to main
                    intent = new Intent(getApplicationContext(), MainActivity.class); //call About class
                    startActivity(intent);
                    SnackUtil.snack(getApplicationContext(), false, "invite accepted");

                    //TODO: add their installationID and their email to the object

                }
                if (v == noButton) {
                    //return to main
                    intent = new Intent(getApplicationContext(), MainActivity.class); //call About class
                    startActivity(intent);
                    //push they declined
                    ParseMgr.getInstance().parseSendNotification(ParseMgr.getInstance().getOwnersEmail(), "declined");
                    SnackUtil.snack(getApplication(), false, "invite declined");
                }
            }
        };
        yesButton.setOnClickListener(z);
        noButton.setOnClickListener(z);
    }
}
