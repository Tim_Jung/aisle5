package com.dunrite.aisle5.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.dunrite.aisle5.R;
import com.dunrite.aisle5.parse.ParseMgr;
import com.dunrite.aisle5.util.SnackUtil;
import com.dunrite.aisle5.util.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;

public class  SharingActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    public GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 69;
    private TextView userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharing);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        getSupportActionBar().setTitle("Share Settings");
        userEmail = (TextView) findViewById(R.id.usersEmail);
        if(mGoogleApiClient.isConnected())
            userEmail.setText("Your Account:\n" + ParseMgr.getInstance().getOwnersEmail());
        final Button shareButton = (Button) findViewById(R.id.shareButton);
        final Switch shareSwitch = (Switch) findViewById(R.id.togglebutton);
        final FrameLayout layout = (FrameLayout) findViewById(R.id.framelayout);
        final View cover = findViewById(R.id.cover);
        final ImageView cloud = (ImageView) findViewById(R.id.cloud);
        final ListView sharingList = (ListView) findViewById(R.id.sharingUsers);
        final SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        final Button signOutButton = (Button) findViewById(R.id.sign_out_button);

        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        //If sync is on populate the list of sharing users
        if (ParseMgr.getInstance().syncStatus()) {
            ParseMgr.getInstance().retrieveSharingMembers();
        }

        //Decide whether to show the blank cover or not
        if (ParseMgr.getInstance().syncStatus()) {
            cover.setVisibility(View.INVISIBLE);
            cloud.setVisibility(View.INVISIBLE);
            shareButton.setVisibility(View.VISIBLE);
            sharingList.setVisibility(View.VISIBLE);
            shareSwitch.setChecked(true);
        } else {
            cover.setVisibility(View.VISIBLE);
            cloud.setVisibility(View.VISIBLE);
            shareButton.setVisibility(View.INVISIBLE);
            sharingList.setVisibility(View.INVISIBLE);
            shareSwitch.setChecked(false);
        }
        View.OnClickListener z = new View.OnClickListener() {
            EditText theInvEmail = (EditText) findViewById(R.id.shareToEmail);
            //Why is this here?
            //TextView message = (TextView)  findViewById(R.id.inviteMessage);

            @Override
            public void onClick(View v) {
                if (v == shareButton) {
                    //TODO: Uncomment block below when ready for release
                    //TODO: Make sure email is one in the Parse DB
                    //Check to see if trying to share with self
                            /*if(!(theInvEmail.getText().toString().equals(possibleEmail) || theInvEmail.getText().toString().equals("test")))
                                ParseMgr.getInstance().sendInvitation(theInvEmail.getText().toString());
                            else {
                                SnackUtil.warningSnack(getActivity(), false, "You cannot invite yourself");
                            }*/
                    //Check if is a valid email
                    if (ParseMgr.getInstance().isEmailValid(theInvEmail.getText().toString()))
                        ParseMgr.getInstance().sendInvitation(theInvEmail.getText().toString());
                    else {
                        SnackUtil.warningSnack(getApplicationContext(), false, "That is not a valid email address");
                    }
                    //remove focus on the edittext
                    theInvEmail.clearFocus();
                    layout.requestFocus();
                    theInvEmail.getText().clear();
                    //Hide the keyboard
                    try {
                        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {

                    }
                }
                if (v == shareSwitch) {
                    //Toggle sharing on / off
                    if (ParseMgr.getInstance().syncStatus() == false) {
                        cover.setVisibility(View.INVISIBLE);
                        cloud.setVisibility(View.INVISIBLE);
                        shareButton.setVisibility(View.VISIBLE);
                        sharingList.setVisibility(View.VISIBLE);
                        ParseMgr.getInstance().syncToggle(true);
                    } else {
                        cover.setVisibility(View.VISIBLE);
                        cloud.setVisibility(View.VISIBLE);
                        shareButton.setVisibility(View.INVISIBLE);
                        sharingList.setVisibility(View.INVISIBLE);
                        ParseMgr.getInstance().syncToggle(false);
                        onSignOutClicked();
                    }
                    Utils.prefSave(getApplication());
                    //TODO: Notify other people that sharing was turned off
                }
                if (v == signInButton) {
                    onSignInClicked();
                }
                if (v == signOutButton) {
                    onSignOutClicked();
                }
            }
        };
        shareButton.setOnClickListener(z);
        shareSwitch.setOnClickListener(z);
        signInButton.setOnClickListener(z);
        signOutButton.setOnClickListener(z);
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d("onStart", "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("SharingActivity", "onConnectionFailed:" + connectionResult);
    }

    /**
     * On sign in clicked.
     * User pressed the sign in button
     */
    public void onSignInClicked() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * On sign out clicked.
     * User pressed the sign out button
     */
    public void onSignOutClicked() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
        ParseMgr.getInstance().setEmail(null);
        userEmail.setText("");
    }

    //For Google sign-in API
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("SharingActivity", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            ParseMgr.getInstance().setEmail(acct.getEmail());
            userEmail.setText("Your Account:\n" + acct.getEmail());
            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {
            findViewById(R.id.sign_in_button).setVisibility(View.INVISIBLE);
            findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_button).setVisibility(View.INVISIBLE);
        }
    }


    /**
     * Populates List View with the emails the user is sharing their list with.
     */
    public void populateSharedListView() {
        ListView sharingList = (ListView) findViewById(R.id.sharingUsers);
        ArrayList<String> sharingUsers = ParseMgr.getInstance().getSharingUsersList();
        Log.d("Sharing Users", "" + sharingUsers);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.sharedusertextview, sharingUsers);
        sharingList.setAdapter(arrayAdapter);
    }
}
