package com.dunrite.aisle5.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.WindowManager;

import com.dunrite.aisle5.R;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * Activity that is used the first time a user opens the app
 */
public class IntroActivity extends AppIntro2 {
    // Please DO NOT override onCreate. Use init
    @Override
    public void init(Bundle savedInstanceState) {

        // Add your slide's fragments here
        // AppIntro will automatically generate the dots indicator and buttons.
        //addSlide(first_fragment);
        //addSlide(second_fragment);
        //addSlide(third_fragment);
        //addSlide(fourth_fragment);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest
        addSlide(AppIntroFragment.newInstance("Welcome to Aisle5", "The Simple, Material Grocery List",
                R.drawable.ic_launcher, Color.parseColor("#3f51b5")));
        addSlide(AppIntroFragment.newInstance("Unique Art", "See original, delightful images instead of just boring text.",
                R.drawable.ic_launcher, Color.parseColor("#3f51b5")));
        addSlide(AppIntroFragment.newInstance("Ad-Free", "No advertisements to distract you from the app.",
                R.drawable.ic_launcher, Color.parseColor("#3f51b5")));

        // OPTIONAL METHODS
        // Override bar/separator color
        //setBarColor(Color.parseColor("#3F51B5"));
        //setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button
        //showSkipButton(false);
        showDoneButton(true);

        // Turn vibration on and set intensity
        // NOTE: you will probably need to ask VIBRATE permesssion in Manifest
        setVibrate(true);
        setVibrateIntensity(30);
    }

    @Override
    public void onDonePressed() {
        Intent intent = new Intent(this, MainActivity.class); //call Intro class
        startActivity(intent);
    }
}